@echo off
:: Accepts a jpeg as input and returns it, optimized. The conversion is lossy,
:: but the quality is set to 95 by default so the loss is minimal. This uses
:: Mozilla's mozjpeg, but they don't build official binaries: skim through this
:: (github.com/mozilla/mozjpeg/issues/91) issue for links to precompiled windows
:: binaries of dubious origin.

:Loop
    %LocalAppData%\Programs\mozjpeg\cjpeg.exe -quality 95 -optimize^
    -progressive "%~1" >"%~dpn1-moz.jpeg"

    shift
    if not "%~1"=="" goto Loop