@echo off
:: Accepts a jpeg as input and returns it, converted to png. The conversion is
:: lossy, but the quality is set to 95 by default so the loss is minimal.
:: GraphicsMagick or ImageMagick are better maintained for Windows, but I prefer
:: NetPBM anyway. The newest Windows binaries I can find are here
:: (onedrive.live.com/?cid=9E7DB242359A93F0&id=9E7DB242359A93F0%2128283).

:Loop
    :: NetPBM can't directly convert from png to jpeg.
    %LocalAppData%\Programs\netpbm\bin\pngtopnm.exe "%~1" >"%~dpn1.pnm"
    %LocalAppData%\Programs\netpbm\bin\pnmtojpeg.exe -optimize -progressive^
    -quality 95 "%~dpn1.pnm" >"%~dpn1.jpeg"
    del "%~dpn1.pnm"

    shift
    if not "%~1"=="" goto Loop