@echo off
:: Accepts a gif and returns a webm video of reasonable quality. The bitrate
:: should probably be set manually for each file, but 2M is an acceptable
:: compromise for my gif collection. This can alter the playback speed of the
:: converted gif. Download ffmpeg binaries from the official site (ffmpeg.org).

:Loop
    %LocalAppData%\Programs\ffmpeg\bin\ffmpeg.exe -y -i "%~1" -c:v libvpx -b:v^
    2M -auto-alt-ref 0 "%~dpn1.webm"

    shift
    if not "%~1"=="" goto Loop