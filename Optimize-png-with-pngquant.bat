@echo off
:: Accepts a png as input and returns it, optimized by pngquant (pngquant.org).
:: This also strips metadata.

:Loop
    %LocalAppData%\Programs\pngquant\pngquant.exe -strip "%~1"

    shift
    if not "%~1"=="" goto Loop