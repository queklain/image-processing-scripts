@echo off
:: Accepts an image and returns it, doubled in size using the waifu2x algorithm.
:: The executable may have a different name. I use the improved fork found at
:: github.com/DeadSix27/waifu2x-converter-cpp.

:Loop
    :: waifu2x-converter-cpp needs to be called from its own directory.
    cd "%LocalAppData%\Programs\waifu2x\"
    waifu2x.exe -j 2 --scale_ratio 2 --noise_level 1 -i "%~1" -o^
    "%~dpn1-waifu%~x1"
    cd /D  "%~dp0"

    shift
    if not "%~1"=="" goto Loop