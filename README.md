Image Processing batch files for Windows
========================================
A small collection of Windows batch files for processing images, polished up and
documented. Each should be self-explanatory, and all require external programs
listed in the comments.

I store programs without installers in %LocalAppData%\Programs in respective
directories, so the batch files expect the same. These scripts are probably
best placed in %LocalAppData%\Programs\image-processing (or something), with
shortcuts added to the SendTo folder. This allows batch processing of images
without a command window.
