@echo off
:: Accepts a jpeg as input and returns it, converted to png. GraphicsMagick or
:: ImageMagick are better maintained for Windows, but I prefer NetPBM
:: anyway. The newest Windows binaries I can find are here
:: (onedrive.live.com/?cid=9E7DB242359A93F0&id=9E7DB242359A93F0%2128283).

:Loop
    :: NetPBM can't directly convert from jpeg to png.
    %LocalAppData%\Programs\netpbm\bin\jpegtopnm.exe "%~1" >"%~dpn1.pnm"
    %LocalAppData%\Programs\netpbm\bin\pnmtopng.exe "%~dpn1.pnm" >"%~dpn1.png"
    del "%~dpn1.pnm"

    shift
    if not "%~1"=="" goto Loop