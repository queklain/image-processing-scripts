@echo off
:: Accepts a png as input and returns it, optimized by optipng
:: (optipng.sourceforge.net).

:Loop
    %LocalAppData%\Programs\optipng\optipng.exe "%~1" -out "%~dpn1-opt%~x1"

    shift
    if not "%~1"=="" goto Loop