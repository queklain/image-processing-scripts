@echo off
:: Accepts a jpeg as input and returns it without the EXIF information.
:: Download the exiftool binary from the official site
:: (sno.phy.queensu.ca/~phil/exiftool/).

:Loop
    %LocalAppData%\Programs\exiftool\exiftool.exe -all= -overwrite_original^
    "%~1"

    shift
    if not "%~1"=="" goto Loop